var map = document.querySelector('#map');
var paths = document.querySelectorAll('#map-image a');
var links = document.querySelectorAll('#map-list a');


//pollyfile du forEach
if(NodeList.prototype.forEach === undefined){
  NodeList.prototype.forEach = function(callback){
    [].forEach.call(this, callback);
  }
}

var activeClass = function(id){
  map.querySelectorAll('.is-active').forEach(function(item){
    item.classList.remove('is-active');
  });
  if(id !== undefined){
    document.querySelector('#' + id).classList.add('is-active');
    document.querySelector('#list-' + id).classList.add('is-active');
  }
}

paths.forEach(function(path){
  path.addEventListener('mouseenter', function(){
    var id = this.id;
    activeClass(id);
  });
});

links.forEach(function(link){
  link.addEventListener('mouseenter', function(e){
    var id = this.id.replace('list-', '');
    activeClass(id);
  });
});

map.addEventListener('mouseover', function(){
  activeClass();
});

//NavDesktop
$(document).scroll(function(){
    var pos = $('header').position();
    var position = document.querySelector('header');
    var positionHeight = position.getBoundingClientRect().bottom;
      console.log(positionHeight);

    if (positionHeight <= 0){
        $("#navdesktop").css('background-color', '#fff');
        $("#navdesktop ul li a").hover(function(){$(this).css("color", "#6BBED5");}, function(){$(this).css("color", "#2c3840");});
        $("#navdesktop ul li a").css('color', '#2c3840');
        $("#navdesktop .top-bar-title a").hover(function(){$(this).css("color", "#6BBED5");}, function(){$(this).css("color", "#2c3840");});
        $("#navdesktop .top-bar-title a").css('color', '#2c3840');

    } else {
        $("#navdesktop").css('background-color', 'transparent');
        $("#navdesktop ul li a").hover(function(){$(this).css("color", "#6BBED5");}, function(){$(this).css("color", "#fff");});
        $("#navdesktop ul li a").css('color', '#fff');
        $("#navdesktop .top-bar-title a").hover(function(){$(this).css("color", "#6BBED5");}, function(){$(this).css("color", "#fff");});
        $("#navdesktop .top-bar-title a").css('color', '#fff');
    }
});
